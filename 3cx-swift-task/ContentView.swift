//
//  ContentView.swift
//  SwiftUI-Task
//
//  Created by Efthymios Zeinis on 21/03/2021.
//  Copyright © 2021 Efthimis. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var formViewModel = FormViewModel()
    
    var body: some View {
        ZStack {
            BackgroundView()
            
            VStack {
                Text("Create an Account")
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .font(.system(size: 32, weight: .bold))
                    .padding(.bottom, 40)
            
                Spacer()
                
                Image(systemName: "person.circle.fill")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 180, height: 80)
                    .padding(.bottom, 40)
                
                FormInputView(inputValue: $formViewModel.username, placeholder: "Username", secured: false, isValid: formViewModel.isValidUsername)
                FormInputView(inputValue: $formViewModel.password, placeholder: "Password", secured: true, isValid: formViewModel.isValidPassword)
                FormInputView(inputValue: $formViewModel.verifyPassword, placeholder: "Verify password", secured: true, isValid: formViewModel.isValidPassword)
                
                Button(action: {}) {
                    RoundedRectangle(cornerRadius: 10)
                        .frame(height: 60)
                    .overlay(
                        Text("Create an Account")
                            .foregroundColor(.white)
                    )
                }
                .padding(.vertical, 20)
                .disabled(!formViewModel.isFormValid)
                
                Spacer()
                        
            }.padding(25)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct BackgroundView: View {
    var body: some View {
        Color("lightGray")
            .edgesIgnoringSafeArea(.all)
    }
}

struct FormInputView: View {
    @Binding var inputValue: String
    var placeholder: String
    var secured: Bool
    var isValid: Bool
    
    var body: some View {
        VStack {
            HStack{
                if(secured){
                    SecureField(placeholder, text: $inputValue)
                        .foregroundColor(.black)
                }else{
                    TextField(placeholder, text: $inputValue)
                        .foregroundColor(.black)
                }
            }
            
            Divider()
                .frame(height: 1)
                .background(!isValid ? Color("borderGray") : .green)
        }.padding(.bottom, 10)
    }
}
