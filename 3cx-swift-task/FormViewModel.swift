//
//  RegisterUserModel.swift
//  SwiftUI-Task
//
//  Created by Efthymios Zeinis on 22/03/2021.
//  Copyright © 2021 Efthimis. All rights reserved.
//

import Foundation
import Combine

class FormViewModel: ObservableObject {
    
    @Published var username = ""
    @Published var password = ""
    @Published var verifyPassword = ""
    
    @Published var isValidUsername = false
    @Published var isValidPassword = false
    @Published var isFormValid = false
    
    private var anyCancellableSet = Set<AnyCancellable>()
    
    // MARK: Publishers
    
    //publisher that invokes a request to detect if the provided username is valid
    var isUsernameValidPublisher: AnyPublisher<Bool, Never> {
      return $username
        .debounce(for: 0.5, scheduler: RunLoop.main)
        .removeDuplicates()
        .flatMap { username in
          return Future { promise in
            self.usernameAvailable(username) { available in
              promise(.success(available))
            }
          }
      }
      .eraseToAnyPublisher()
    }
    
    //publisher to determine if the password is valid based on its length
    private var isPasswordEmptyPublisher: AnyPublisher<Bool, Never> {
        $password
            .removeDuplicates()
            .map { $0.count >= 8 }
            .eraseToAnyPublisher()
    }
    
    //publisher to determine if the password is valid based on its uniqueness
    private var isPasswordUniquePublisher: AnyPublisher<Bool, Never> {
        $password
            .removeDuplicates()
            .map { $0 != "password" && $0 != "admin" }
            .eraseToAnyPublisher()
    }
    
    //publisher to determine if the password and verify password values are the same
    private var arePasswordsSamePublisher: AnyPublisher<Bool, Never> {
        Publishers.CombineLatest($password, $verifyPassword)
            .map { $0 == $1 }
            .eraseToAnyPublisher()
    }
    
    //publisher that combines all the password validation ones
    private var isPasswordValidPublisher: AnyPublisher<Bool, Never> {
        Publishers.CombineLatest3(isPasswordEmptyPublisher, isPasswordUniquePublisher, arePasswordsSamePublisher)
            .map {
                return $0 && $1 && $2
            }
            .eraseToAnyPublisher()
    }
    
    //published that combines the username and password valiation publishers to determine whether the form is valid or not
    private var isFormValidPublisher: AnyPublisher<Bool, Never> {
        Publishers.CombineLatest(isPasswordValidPublisher, isUsernameValidPublisher)
            .map { $0 && $1 }
            .eraseToAnyPublisher()
    }
    
    //constructor
    init() {
        isUsernameValidPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.isValidUsername, on: self)
            .store(in: &anyCancellableSet)
        
        isPasswordValidPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.isValidPassword, on: self)
            .store(in: &anyCancellableSet)
        
        isFormValidPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.isFormValid, on: self)
            .store(in: &anyCancellableSet)
    }
    
    // MARK: Helpers
    func usernameAvailable(_ username: String, completion: (Bool) -> Void) {
        username.count < 5 ? completion(false) : completion(true)
    }
}
